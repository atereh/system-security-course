#!/bin/bash

rm -rf ./maze

mkdir -p maze
cd maze

chains=31
nested_dirs=400
home=$(pwd)

target_file_path="/public"

result=""


dir_stack=""
for ((j = 0; j < $nested_dirs; j++))
do
  dir_stack="$dir_stack/d"
done


for ((i = 0; i < $chains; i++))
do
  mkdir -p "$home/$i/$dir_stack"
  cd "$home/$i/$dir_stack"

  if [[ "$i" == "0" ]]
  then
    ln -s $target_file_path lnk
  else
    ln -s "$home/$(($i-1))/$dir_stack" lnk
  fi
  result="$result/lnk"

done

cd $home
ln -s "$home/$(($chains-1))/$dir_stack" entry
result="$home/entry/$result" 

echo $result