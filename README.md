# System security tasks from EURECOM course #

This repo contains different algorithms for solving security tasks. Tasks are taken from EURECOM SysSec course Fall 2020.

### Challenge 01 ###
Shell Attack, over-riding the shell 'cat' command.

SOLUTION TO BE UPLOADED SOON

### Challenge 02 ###

In **chall02** folder you can find my bash script for generating a maze of nested folders in the local directory.
It creates a directory called **maze**, then it creates a (chain) number of folders in it with (nested_dirs) number of directories in each folder. 

Each last directory in the chain (folder) contains a **symbolic link** to the next chain. The very last directory contains a symbolic link to a public file.

Then script echo's you a **result** - the full link to a nested public file.

As a result we get a file system maze by raising total file system lookups. Our malicious **result** filename forces the OS to traverse the entire chain of symbolic links.

**Scheme:**

![file system maze scheme](https://i.imgur.com/nHAcSlg.png = 60x60)

===================

**Race condition challenge**

**scat.c** is an example of a program vulnerable to a file race. The
file race attack allows to read the contents of the "private" file
form a normal user account by leveraging the file race vulerability in
the **scat** program.


You can copy the files in copy_to_your_homedir to your home dir to be
able to use/modify them. 
But the scat program, private file and the
**clear_cache** program must be used in place.

**The work consists in :**

1) Writing a script to build the maze.

**Solved: folders_generator.sh script in folder chall02.**

2) Complete the attack script that should change the target of the link (and possibly the restore script, which is handy is the attack failed for some reason).

3) Run the attack.

4) Obtain the token in the private file and submit it as a file containing the token only using the comand : ```submit challXY tokenfile```

Then please clean your maze, those files are quickly filling the disk (it's inodes actually).


